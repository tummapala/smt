angular.module('shopMyTools.controllers', [])

    .controller('welcomeController', function ($scope, $state) {

        $scope.options = {
            loop: false,
            effect: 'slide', // cube, coverflow
            speed: 1500,
        }

        $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
            // data.slider is the instance of Swiper
            $scope.slider = data.slider;
        });

        $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
            console.log('Slide change is beginning');
        });

        $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
            // note: the indexes are 0-based
            $scope.activeIndex = data.slider.activeIndex;
            $scope.previousIndex = data.slider.previousIndex;
        });

        $scope.gotoLoginPage = function () {
            $state.go('smtLogin');
        };
    })

    .controller('loginController', function ($scope, $rootScope, $state, loginService, $ionicPopup, $timeout) {

        $scope.loginData = {};


        $scope.gotoRegistration = function () {
            $state.go('smt_registration');
        };

        $scope.login = function (form) {
            if (form.$valid) {
                loginService.userAuthentication($scope.loginData.username, $scope.loginData.password).then(function (data) {

                    if (data.data.status == 'Success') {
                        $rootScope.userId = data.data.user_id;
                        $state.go('welcome');
                    } else {
                        $ionicPopup.alert({
                            template: data.data.status,
                            title: 'Error!'
                        });
                    }
                });
            }
        };

        $scope.gotoForgotPswd = function () {
            $state.go('forgotPassword');
        };
    })

    .controller('registrationController', function ($scope, $rootScope, registrationService, $state, $ionicPopup) {
        //alert('ok');
        $scope.registerData = {};

        $scope.registerData.newsletter = "unchecked";
        $scope.registerData.gstnumber = "";


        $scope.getOtp = function (form) {
            $rootScope.mobile = $scope.registerData.mobile;
            if (form.$valid && $scope.registerData.password == $scope.registerData.confirm_password) {
                registrationService.getOtp($scope, $rootScope).then(function (data) {
                    alert(data.data.status);
                    if (data.data.status == 'Data saved successfully') {
                        $rootScope.myPopup = $ionicPopup.show({
                            templateUrl: 'templates/otpPopup.html',
                            title: 'Enter OTP'
                        });
                    }

                });
            }
        };


        $scope.verifyOTP = function (form, otp) {
            if (form.$valid) {
                registrationService.verifyOTP(otp, $rootScope.mobile).then(function (data) {

                    if (data.data.return == 'otp verified') {
                        $rootScope.myPopup.close();

                        $state.go('smtLogin');
                    } else {

                        $ionicPopup.alert({
                            template: data.data.return,
                            title: 'Error!'
                        });
                    }

                })
            }

        }

        $scope.resendOTP = function () {
            registrationService.resendOTP($rootScope.userId).then(function (data) {
                // alert(JSON.stringify(data))
                $ionicPopup.alert({
                    template: data.data.return,
                    title: 'Success!'
                });

            })
        }

        $scope.gotoLogin = function () {
            $state.go('smtLogin');
            // $state.go('app.home');
        };
    })



    .controller('forgotPasswordCtrl', function ($scope, $rootScope, forgotPaswdService, $state, $ionicPopup) {

        $scope.forgetPswdData = {};

        $scope.forgetPswd = function (form) {
            if (form.$valid) {
                // $state.go('emailSent');
                forgotPaswdService.forgotPassword($scope).then(function (data) {
                    if (data.data.status == 'email sent') {
                        $state.go('emailSent');
                    } else {
                        $ionicPopup.alert({
                            template: data.data.status,
                            title: 'Error!'
                        });
                    }

                })

            }
        };
        $scope.gotoLogin = function () {
            $state.go('smtLogin');
        };
    })


    .controller('menuController', function ($scope, $state) {

        marqueeInit({
            uniqueid: 'mycrawler',
            inc: 2, //speed - pixel increment for each iteration of this marquee's movement
            mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
            moveatleast: 2,
            neutral: 150,
            savedirection: true,
            random: true
        });

        $scope.gotoProductPage = function (category) {
            if (category == '1') {
                $state.go('login');
            } else if (category == '2') {
                $state.go('app.invoiceOrders');
            } else if (category == '3') {
                $state.go('app.myorders');
            } else if (category == '4') {
                $state.go('app.home');
            }
        }


    });